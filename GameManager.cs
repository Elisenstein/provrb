﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System;
using Random = UnityEngine.Random;
using System.ComponentModel;

public class GameManager : MonoBehaviour
{
    public bool rightHand = false; //false is left, true is right

    public GameObject ball;
    public GameObject cube;
    Collider m_Collider1;
    Collider m_Collider2;

    private bool reset = false; //reset is whether the hand is in the phase of the trial reaching for the ball, or before the next ball has spawned
                                // private bool cubecollision = false;
    private int trial = 0;
    private float r = 0.4f;
    private float theta;
    private double error;
    private float[] initCoords;
    //private UnityEngine.Random rnd = new UnityEngine.Random();
    private Vector3 corVec = new Vector3(0.1f, 0.1f, 0.1f);
    private GameObject OVRCameraRig;
    private GameObject TrackingSpace;
    private GameObject LeftHandAnchor;
    private GameObject RightHandAnchor;
    private GameObject LeftFab;
    private GameObject RightFab;
    private GameObject Hand;
    private GameObject DisplayText;
    private GameObject Degree;
    private GameObject Error;
    private GameObject Trial;
    private GameObject Visibility;
    private GameObject BallPos;
    private GameObject HandPos;
    private GameObject Pinch;

    public static GameManager Instance;

    [SerializeField] private OVRSkeleton skeleton;
    private OVRHand hand;
    private Transform tip;

    private int[,] expOrder = new int[25, 2];
    private float[,,] data = new float[5, 5, 2];
    public string path;
    public int email_sent;
    public int hand_visible;





    //Create text file that contains the erros for each trial
    void CreateText()
    {
        //Path of the file to save to Oculus Quest internal files
        path = "./sdcard/Android/data/com.TadinLab.ProVRb/Data" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".txt";
        email_sent = 0;

        //Create file if it doesn't exist
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "List of Errors by Trial \n\n");
        }
    }



    // Start is called before the first frame update
    void Start()
    {
       



        //////
        {
            CreateText(); //calls the function to create the text file once the experiment starts

        }

        if (Instance == null)
        {
            Instance = this;
        }

        //init_coords[0] = CenterEye.Instance.gameObject.transform.position.x;
        //init_coords[1] = CenterEye.Instance.gameObject.transform.position.y;
        //init_coords[2] = CenterEye.Instance.gameObject.transform.position.z;



        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                expOrder[i * 5 + j, 0] = i;
                expOrder[i * 5 + j, 1] = j;
            }
        }


        Shuffle();

        OVRCameraRig = GameObject.Find("OVRCameraRig");
        TrackingSpace = OVRCameraRig.transform.GetChild(0).gameObject;
        LeftHandAnchor = TrackingSpace.transform.GetChild(4).gameObject;
        RightHandAnchor = TrackingSpace.transform.GetChild(5).gameObject;
        LeftFab = LeftHandAnchor.transform.GetChild(1).gameObject;
        RightFab = RightHandAnchor.transform.GetChild(1).gameObject;



        DisplayText = GameObject.Find("DisplayText");
        Visibility = DisplayText.transform.GetChild(0).gameObject;
        ///Degree = DisplayText.transform.GetChild(0).gameObject;
        ///Error = DisplayText.transform.GetChild(1).gameObject;
        Trial = DisplayText.transform.GetChild(2).gameObject;
        ///HandPos = DisplayText.transform.GetChild(3).gameObject;
        ///BallPos = DisplayText.transform.GetChild(4).gameObject;
        ///Pinch = DisplayText.transform.GetChild(5).gameObject;

        Visibility.GetComponent<TextMesh>().text = "vis: n/a";
        ///Degree.GetComponent<TextMesh>().text = "deg: n/a";
        ///Error.GetComponent<TextMesh>().text = "err: n/a";
        Trial.GetComponent<TextMesh>().text = "tri: 1";


        if (rightHand == false)
        {

            Hand = LeftHand.Instance.gameObject;
            skeleton = LeftFab.GetComponent<OVRSkeleton>();
            hand = LeftFab.GetComponent<OVRHand>();
            RightFab.gameObject.SetActive(false);


        }

        else
        {

            Hand = RightHand.Instance.gameObject;
            skeleton = RightFab.GetComponent<OVRSkeleton>();
            hand = RightFab.GetComponent<OVRHand>();
            LeftFab.gameObject.SetActive(false);

        }

        





        //Instantiate(RightFab.GetComponent<OVRMeshRenderer>());


        // THIS ONE TURNS OFF HAND MESH
        //{
        //    RightFab.GetComponent<OVRMeshRenderer>().enabled = false; //makes hand invisible
        //}





        theta = UnityEngine.Random.Range(110f, 200f) * Mathf.PI / 180f;
        theta = ((expOrder[trial, 0] + 1) * 20 + 30) * Mathf.PI / 180f;
        Instantiate(ball, new Vector3(r * Mathf.Cos(theta), 0, r * Mathf.Sin(theta)), Quaternion.identity);
        ///Degree.GetComponent<TextMesh>().text = "deg: " + ((expOrder[trial, 0] + 1) * 20 + 30).ToString();
        //Instantiate(cube, new Vector3(0f, 0f, .2f), Quaternion.identity);

        //HandPos.GetComponent<TextMesh>().text = fingerBones.Count.ToString();
        //Instantiate(cube, new Vector3(0, 0, 0), Quaternion.identity);


        tip = skeleton.Bones[(int)OVRSkeleton.BoneId.Hand_IndexTip].Transform;

        hand_visible = 0;


    }


    // Update is called once per frame
    void Update()

    {



        /// BallPos.GetComponent<TextMesh>().text = System.Math.Round(Ball.Instance.gameObject.transform.position.x, 2).ToString() + ","
        ///                                      + System.Math.Round(Ball.Instance.gameObject.transform.position.z, 2).ToString() + ",";


        /// HandPos.GetComponent<TextMesh>().text = System.Math.Round(tip.transform.position.x, 2).ToString() + ","
        ///                                     + System.Math.Round(tip.transform.position.z, 2).ToString() + ",";

        /// Pinch.GetComponent<TextMesh>().text = hand.GetFingerIsPinching(OVRHand.HandFinger.Index).ToString();


        //theta = Random.Range(40f, 160f) * Mathf.PI / 180f;
        //Ball.Instance.gameObject.transform.position = new Vector3(r * Mathf.Cos(theta), 0, r * Mathf.Sin(theta));



        //if (hand_visible == 0)
        //{
        //    RightFab.GetComponent<OVRMeshRenderer>().enabled = false; //makes hand invisible
        //}

        ////else
        //if (hand_visible == 1)
        //{
        //    RightFab.GetComponent<OVRMeshRenderer>().enabled = true; //makes hand visible
        //}

        if ((tip.transform.position).magnitude >= r && (reset == false) && trial < 25)
        {
            //int randomValue = Random.Range(0, 2);
            //hand_visible = randomValue;
            hand_visible = Random.Range(0, 2);

            if (hand_visible == 0)
            {
                //Destroy(RightFab.GetComponent<OVRMeshRenderer>());
                //Instantiate(RightFab.GetComponent<OVRMeshRenderer>());
                RightFab.GetComponent<OVRMeshRenderer>().enabled = false; //makes hand invisible
            }



            //else
            if (hand_visible == 1)
            {

                RightFab.GetComponent<OVRMeshRenderer>().enabled = true; //makes hand visible
            }

            //error = Vector3.Distance(Ball.Instance.gameObject.transform.position, tip.transform.position);
            error = Vector3.Angle(Ball.Instance.gameObject.transform.position, tip.transform.position);


            error = System.Math.Round(error, 3);

            reset = true;
            Ball.Instance.gameObject.transform.localScale = new Vector3(0f, 0f, 0f);

            Instantiate(cube, new Vector3(0f, 0f, .2f), Quaternion.identity);



            trial++;

           

            Visibility.GetComponent<TextMesh>().text = "vis: " + hand_visible.ToString();
            ///Error.GetComponent<TextMesh>().text = "err: " + error.ToString();
            Trial.GetComponent<TextMesh>().text = "tri: " + (trial + 1).ToString();
            //Cube.Instance.gameObject.transform.position = new Vector3(0, 0, .2f);


            {

                //Content of the previously created text file
                string content = "Error: " + error + "\r";
                //Add some text to it by appending a new line after each trial
                File.AppendAllText(path, content);



            }

        }




        // run this script once at the 25th trials and the email has not been sent yet (this prevents an email from being sent during every frame of the 25th trial)
        if (trial == 25 && email_sent == 0)
        {
            //must enable SMTP on the specified gmail account 


            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("elisenstein@gmail.com");
            //enter SMTP enabled gmail address above to send FROM
            mail.To.Add("elisenstein@gmail.com");
            //enter SMTP enabled gmail address above to send TO
            mail.Subject = "Test Smtp Mail";
            mail.IsBodyHtml = true; //to make message body as html  
            mail.Body = "Data";
            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(path); //********
            mail.Attachments.Add(attachment);

            // you can use others too.
            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("elisenstein@gmail.com", "BRMBhf47") as ICredentialsByHost;
            //enter SMTP enabled gmail account above NetworkCredential("emailaddress@gmail.com", "password for email address")
            smtpServer.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };
            smtpServer.Send(mail);
            email_sent = 1;

        }



        {
            if ((tip.transform.position).magnitude <= .5 * r && (reset == true) && trial < 25)
            {

                // hand_visible = 0;

                reset = false;
                theta = ((expOrder[trial, 0] + 1) * 20 + 30) * Mathf.PI / 180f;
                Ball.Instance.gameObject.transform.localScale = new Vector3(0.025f, 0.025f, 0.025f);

                Destroy(Cube.Instance.gameObject);

                Ball.Instance.gameObject.transform.position = new Vector3(r * Mathf.Cos(theta), 0, r * Mathf.Sin(theta));
                // Cube.Instance.gameObject.transform.position = new Vector3(0, 0, .5f);

                ///Degree.GetComponent<TextMesh>().text = "deg: " + ((expOrder[trial, 0] + 1) * 20 + 30).ToString();

              

            }
        }

       

        if (trial >= 25)
        {

            Ball.Instance.gameObject.transform.localScale = new Vector3(0f, 0f, 0f);

        }


    }

    public void Shuffle()
    {
        int rand; ;
        int tempOne;
        int tempTwo;

        for (int i = 0; i < expOrder.GetLength(0); i++)
        {
            rand = UnityEngine.Random.Range(0, expOrder.GetLength(0) - 1);

            tempOne = expOrder[rand, 0];
            tempTwo = expOrder[rand, 1];

            expOrder[rand, 0] = expOrder[i, 0];
            expOrder[rand, 1] = expOrder[i, 1];

            expOrder[i, 0] = tempOne;
            expOrder[i, 1] = tempTwo;

        }
    }
}
